from flask import jsonify
from app import app, mysql, jwt
from flask_jwt_extended import jwt_required


@jwt.unauthorized_loader
def unnautorized(callback):
    return jsonify({'ok': False, 'message': 'Missing Authorization Header'}), 401


@app.route('/Lojas', methods=['GET'])
@jwt_required
def getLoja():
    request_data = []
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM Loja")
    data = cursor.fetchall()
    if len(data) > 0:
        for i in data:
            request_data.append({'id': i[0], 'nome': i[1], 'code': i[2], 'regiao': i[3], 'slug': i[4] })
        return jsonify({"ok": True, "data": list(request_data)}), 200
    return jsonify({"ok": False, "message": "Loja is not Found"}), 400


@app.route('/Loja/<id>', methods=['GET'])
@jwt_required
def getLojaId(id):
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM Loja WHERE id = %s" % id)
    i = cursor.fetchone()
    if i:
        request_data = { 'id': i[0], 'nome': i[1], 'code': i[2], 'region': i[3], 'slug': i[4] }
        return jsonify({"ok": True, "data": request_data}), 200
    return jsonify({"ok": False, "message": "Loja is not Found"}), 400


@app.route('/Loja/code/<codigo>', methods=['GET'])
@jwt_required
def getLojaCode(codigo):
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM Loja WHERE cod_loja = %s" % codigo)
    i = cursor.fetchone()
    if i:
        request_data = { 'id': i[0], 'nome': i[1], 'code': i[2], 'region': i[3], 'slug': i[4] }
        return jsonify({"ok": True, "data": request_data}), 200
    return jsonify({"ok": False, "message": "Loja is not Found"}), 400


@app.route('/Loja/slug/<slug>', methods=['GET'])
@jwt_required
def getLojaSlug(slug):
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM Loja WHERE slug='%s'" % str(slug))
    i = cursor.fetchone()
    if i:
        request_data = { 'id': i[0], 'nome': i[1], 'code': i[2], 'region': i[3], 'slug': i[4] }
        return jsonify({"ok": True, "data": request_data}), 200
    return jsonify({"ok": False, "message": "Loja is not Found"}), 400


@app.route('/Loja/region/<region>', methods=['GET'])
@jwt_required
def getLojaRegion(region):
    request_data = []
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM Loja WHERE regiao = %s" % region)
    data = cursor.fetchall()
    if len(data) > 0:
        for i in data:
            request_data.append({'id': i[0], 'nome': i[1], 'code': i[2], 'region': i[3], 'slug': i[4]})
        return jsonify({"ok": True, "data": list(request_data) }), 200
    return jsonify({"ok": False, "message": "Loja is not Found"}), 400
