from flask import jsonify, request
from app import app, mysql, bcrypt, jwt
from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required,
                                jwt_refresh_token_required, get_jwt_identity)


@jwt.unauthorized_loader
def unnautorized(callback):
    return jsonify({'ok': False, 'message': 'Missing Authorization Header'}), 401


@app.route('/register', methods=['POST'])
def register():
    success = []
    conn = mysql.connect()
    cursor = conn.cursor()
    filter = conn.cursor()
    data = request.get_json()
    if data:
        filter.execute(f"SELECT COUNT(id) FROM Flask WHERE email='{data['email']}'")
        exist = filter.fetchone()
        if exist[0] < 1:
            data['password'] = bcrypt.generate_password_hash(data['password']).decode('utf-8')
            print( data )
            query = "INSERT INTO Flask (name, email, password) VALUES ('%s', '%s', '%s')" % \
                    (data['name'], data['email'], data['password'])
            cursor.execute(query)
            conn.commit()
            success = { 'name': data['name'], 'email': data['email'] }
            return jsonify({'ok': True, 'message': 'User created successfully!', "data": success}), 200
        else:
            return jsonify({'ok': True, 'message': 'User exist!'}), 400
    else:
        filter.close()
        cursor.close()
        conn.close()
        return jsonify({'ok': False, 'message': 'Bad request parameters: {}'.format(data['message'])}), 400


@app.route('/auth', methods=['POST'])
def auth_user():
    data = request.get_json()
    cursor = mysql.connect().cursor()
    if data:
        cursor.execute("SELECT * FROM Flask WHERE email ='%s'" % data['email'])
        alpha = cursor.fetchone()
        user = {"name": alpha[1], "email": alpha[2], "password": alpha[3] }
        if user and bcrypt.check_password_hash(user["password"], data["password"]):
            del user["password"]
            access_token = create_access_token(identity=data)
            refresh_token = create_refresh_token(identity=data)
            user['token'] = access_token
            user['refresh'] = refresh_token
            return jsonify({'ok': True, 'data': user}), 200
        else:
            return jsonify({'ok': False, 'message': 'invalid username or password'}), 401
    else:
        return jsonify({'ok': False, 'message': 'Bad request parameters: {}'.format(data['message'])}), 400
    pass


@app.route('/refresh', methods=['POST'])
@jwt_refresh_token_required
def refresh():
    current_user = get_jwt_identity()
    print(current_user)
    ret = {'token': create_refresh_token(identity=current_user)}
    return jsonify({'ok': True, 'data': ret}), 200


@app.route('/user', methods=['POST'])
@jwt_required
def getUser():
    data = request.get_json()
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT id, name, email FROM Flask WHERE email = '%s'" % data['email'])
    user = cursor.fetchone()
    if user:
        res = {"id": user[0], "name": user[1], "email": user[2] }
        return jsonify({'ok': True, 'data': res}), 200
    return jsonify({"ok": False, "message": "User not exist"} ), 400

