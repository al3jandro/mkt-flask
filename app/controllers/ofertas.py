import os
from flask import jsonify
from app import app, mysql


@app.route('/Ofertas', methods=['GET'])
def getOfertas():
    request_data = []
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM Ofertas")
    data = cursor.fetchall()
    if len(data) > 0:
        for i in data:
            request_data.append({
                "id": i[0],
                "campanhaId": i[1],
                "campanha": i[2],
                "hostId": i[3],
                "host": i[4],
                "loja": i[5],
                "preco_regular": str(i[6]),
                "parcela_regular": str(i[7]),
                "qtd_regular": i[8],
                "preco_clube": str(i[9]),
                "parcela_clube": str(i[10]),
                "qtd_clube": i[11],
                "dsc_kit": i[12],
                "cod_kit": i[13]
            })
    return jsonify(list(request_data)), 200


@app.route('/Ofertas/campanha/<campanha>', methods=['GET'])
def getOfertasCampanha(campanha):
    request_data = []
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM Ofertas WHERE campanha = %s" % campanha)
    data = cursor.fetchall()
    if len(data) > 0:
        for i in data:
            request_data.append({
                "id": i[0],
                "campanhaId": i[1],
                "campanha": i[2],
                "hostId": i[3],
                "host": i[4],
                "loja": i[5],
                "preco_regular": str(i[6]),
                "parcela_regular": str(i[7]),
                "qtd_regular": i[8],
                "preco_clube": str(i[9]),
                "parcela_clube": str(i[10]),
                "qtd_clube": i[11],
                "dsc_kit": i[12],
                "cod_kit": i[13]
            })
    return jsonify(list(request_data)), 200


@app.route('/Ofertas/loja/<loja>', methods=['GET'])
def getOfertasLoja(loja):
    request_data = []
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM Ofertas WHERE loja = %s" % loja)
    data = cursor.fetchall()
    if len(data) > 0:
        for i in data:
            request_data.append({
                "id": i[0],
                "campanhaId": i[1],
                "campanha": i[2],
                "hostId": i[3],
                "host": i[4],
                "loja": i[5],
                "preco_regular": str(i[6]),
                "parcela_regular": str(i[7]),
                "qtd_regular": i[8],
                "preco_clube": str(i[9]),
                "parcela_clube": str(i[10]),
                "qtd_clube": i[11],
                "dsc_kit": i[12],
                "cod_kit": i[13]
            })
        return jsonify({"ok": True, "data": list(request_data) }), 200
    return jsonify({"ok": False, "message": "Ofertas not found"}), 400


@app.route('/Ofertas/slug/<slug>', methods=['GET'])
def getOfertasSlug(slug):
    request_data = []
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT Ofertas.*, Campanha.description FROM `Ofertas` "
                   "INNER JOIN Campanha ON Campanha.codigo = Ofertas.campanha "
                   "WHERE Campanha.description = '%s'" % slug)
    data = cursor.fetchall()
    if len(data) > 0:
        for i in data:
            request_data.append({
                "id": i[0],
                "campanhaId": i[1],
                "campanha": i[2],
                "hostId": i[3],
                "host": i[4],
                "loja": i[5],
                "preco_regular": str(i[6]),
                "parcela_regular": str(i[7]),
                "qtd_regular": i[8],
                "preco_clube": str(i[9]),
                "parcela_clube": str(i[10]),
                "qtd_clube": i[11],
                "dsc_kit": i[12],
                "cod_kit": i[13]
            })
        return jsonify({"ok": True, "data": list(request_data) }), 200
    return jsonify({"ok": False, "message": "Ofertas not found"}), 400


@app.route('/Ofertas/loja/<loja>/slug/<slug>', methods=['GET'])
def getOfertasLojaSlug(loja, slug):
    request_data = []
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT Ofertas.*, Campanha.description FROM `Ofertas` "
                   "INNER JOIN Campanha ON Campanha.codigo = Ofertas.campanha "
                   "WHERE Ofertas.loja = %s AND Campanha.description = '%s'" % (loja, slug))
    data = cursor.fetchall()
    if len(data) > 0:
        for i in data:
            request_data.append({
                "id": i[0],
                "campanhaId": i[1],
                "campanha": i[2],
                "hostId": i[3],
                "host": i[4],
                "loja": i[5],
                "preco_regular": str(i[6]),
                "parcela_regular": str(i[7]),
                "qtd_regular": i[8],
                "preco_clube": str(i[9]),
                "parcela_clube": str(i[10]),
                "qtd_clube": i[11],
                "dsc_kit": i[12],
                "cod_kit": i[13],
                "slug": i[15]
            })
        return jsonify({"ok": True, "data": list(request_data) }), 200
    return jsonify({"ok": False, "message": "Ofertas not found"}), 400


@app.route('/Ofertas/loja/<loja>/campanha/<campanha>', methods=['GET'])
def getOfertasLojaCampanha(loja, campanha):
    request_data = []
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM Ofertas WHERE loja=%s AND campanha=%s" % (loja, campanha))
    data = cursor.fetchall()
    if len(data) > 0:
        for i in data:
            request_data.append({
                "id": i[0],
                "campanhaId": i[1],
                "campanha": i[2],
                "hostId": i[3],
                "host": i[4],
                "loja": i[5],
                "preco_regular": str(i[6]),
                "parcela_regular": str(i[7]),
                "qtd_regular": i[8],
                "preco_clube": str(i[9]),
                "parcela_clube": str(i[10]),
                "qtd_clube": i[11],
                "dsc_kit": i[12],
                "cod_kit": i[13]
            })
    return jsonify(list(request_data)), 200