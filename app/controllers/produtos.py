import os
import json
import base64
import datetime
from flask import jsonify
from app import app, mysql


@app.route('/Produtos', methods=['GET'])
def getProdutos():
    request_data = []
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT id, cod_campanha, slug, cod_produto, ean, image, dsc_produto, dsc_descricao, embalagem,"
                   "departamento, setor, dsc_departamento, dsc_setor, start, end, dsc_kit FROM Produtos")
    data = cursor.fetchall()
    if len(data) > 0:
        for p in data:
            request_data.append({
                "id": p[0],
                "cod_campanha": p[1],
                "slug": p[2],
                "cod_produto": p[3],
                "ean": p[4],
                "image": p[5],
                "dsc_produto": p[6],
                "dsc_descricao": p[7],
                "embalagem": p[8],
                "departamento": p[9],
                "setor": p[10],
                "dsc_departamento": p[11],
                "dsc_setor": p[12],
                "start": p[13],
                "end": p[14],
                "dsc_kit": p[15]
            })
            return jsonify({ "ok": True, "data": list(request_data) }), 200
    return jsonify({"ok": False, "message": "Produtos not found"}), 400


@app.route('/Produto/code/<code>', methods=['GET'])
def getProdutoCode(code):
    path = '%s/public/banco' % os.getcwd()
    directory = os.listdir( path )
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT id, cod_campanha, slug, cod_produto, ean, image, dsc_produto, dsc_descricao, embalagem,"
                   "departamento, setor, dsc_departamento, dsc_setor, start, end, dsc_kit FROM Produtos "
                   "WHERE cod_produto = %s" % code)
    p = cursor.fetchone()
    if p:
        for item in directory:
            f, e = os.path.splitext(item)
            if code == f:
                bs = base64.b64encode(open(f"{path}/{item}", "rb").read()).decode('utf-8')
                request_data = {
                    "id": p[0],
                    "cod_campanha": p[1],
                    "slug": p[2],
                    "cod_produto": p[3],
                    "ean": p[4],
                    "legal": p[5],
                    "dsc_produto": p[6],
                    "dsc_descricao": p[7],
                    "embalagem": p[8],
                    "departamento": p[9],
                    "setor": p[10],
                    "dsc_departamento": p[11],
                    "dsc_setor": p[12],
                    "start": p[13],
                    "end": p[14],
                    "dsc_kit": p[15],
                    "image": bs
                }
                return jsonify({"ok": True, "data": request_data }), 200
    return jsonify({"ok": False, "message": "Produto not found"}), 400


@app.route('/Produto/slug/<slug>', methods=['GET'])
def getProdutoSlug(slug):
    request_data = []
    path = '%s/public/banco' % os.getcwd()
    directory = os.listdir(path)
    cursor = mysql.connect().cursor()
    sql = "SELECT id, cod_campanha, slug, cod_produto, ean, image, dsc_produto, dsc_descricao, embalagem, " \
          "departamento, setor, dsc_departamento, dsc_setor, start, end, dsc_kit FROM Produtos " \
          "WHERE slug='%s'" % str(slug)
    cursor.execute(sql)
    p = cursor.fetchone()
    code = "%s" % p[3]
    if p:
        for item in directory:
            f, e = os.path.splitext(item)
            if code == f:
                bs = base64.b64encode(open(f"{path}/{item}", "rb").read()).decode('utf-8')
                request_data = {
                    "id": p[0],
                    "cod_campanha": p[1],
                    "slug": p[2],
                    "cod_produto": p[3],
                    "ean": p[4],
                    "legal": p[5],
                    "dsc_produto": p[6],
                    "dsc_descricao": p[7],
                    "embalagem": p[8],
                    "departamento": p[9],
                    "setor": p[10],
                    "dsc_departamento": p[11],
                    "dsc_setor": p[12],
                    "start": p[13],
                    "end": p[14],
                    "dsc_kit": p[15],
                    "image": bs
                }
                return jsonify({"ok": True, "data": request_data }), 200
    print( request_data )
    return jsonify({"ok": False, "message": "Produto not found"}), 400



@app.route('/Produto/Image/<code>', methods=['GET'])
def Image(code):
    bs = ''
    path = '%s/public/banco' % os.getcwd()
    directory = os.listdir(path)
    for item in directory:
        f, e = os.path.splitext(item)
        if code == f:
            bs = base64.b64encode(open(f"{path}/{item}", "rb").read()).decode('utf-8')
            data = {"ok": True, "code": code, "image": bs}
            return jsonify(data), 200
    return jsonify({"ok": False, "message": "Image not found"}), 400
