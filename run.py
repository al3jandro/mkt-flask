from flask import jsonify
from app import app


@app.route("/")
@app.route("/index")
def index():
    data = { 'message': 'Index' }
    return jsonify(data), 200


if __name__ == '__main__':
    app.run()
