from flask import jsonify
from app import app, mysql, jwt
from flask_jwt_extended import jwt_required


@jwt.unauthorized_loader
def unnautorized(callback):
    return jsonify({'ok': False, 'message': 'Missing Authorization Header'}), 401


@app.route('/Campanhas', methods=['GET'])
@jwt_required
def getCampanha():
    request_data = []
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM Campanha")
    data = cursor.fetchall()
    if len(data) > 0:
        for i in data:
            request_data.append({'id': i[0], 'codigo': i[1], 'nome': i[2], 'description': i[3],
                                 'produtos_total': i[4], 'produtos_load': i[5], 'data_start': i[6],
                                 'data_end': i[7], 'status': i[8]})
        return jsonify({"ok": True, "data": list(request_data)}), 200
    return jsonify({"ok": False, "message": "Loja is not Found"}), 400


@app.route('/Campanha/<id>', methods=['GET'])
@jwt_required
def getCampanhaId(id):
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM Campanha WHERE id = %s" % id)
    data = cursor.fetchone()
    if data:
        request_data = {
            'id': data[0],
            'codigo': data[1],
            'nome': data[2],
            'description': data[3],
            'produtos_total': data[4],
            'produtos_load': data[5],
            'data_start': data[6],
            'data_end': data[7],
            'status': data[8]
        }
        return jsonify({"ok": True, "data": request_data}), 200
    return jsonify({"ok": False, "message": "Loja is not Found"}), 400


@app.route('/Campanha/code/<codigo>', methods=['GET'])
@jwt_required
def getCampanhaCode(codigo):
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * FROM Campanha WHERE codigo = %s" % codigo)
    data = cursor.fetchone()
    if data:
        request_data = {
            'id': data[0],
            'codigo': data[1],
            'nome': data[2],
            'description': data[3],
            'produtos_total': data[4],
            'produtos_load': data[5],
            'data_start': data[6],
            'data_end': data[7],
            'status': data[8]
        }
        return jsonify({"ok": True, "data": request_data}), 200
    return jsonify({"ok": False, "message": "Loja is not Found"}), 400
