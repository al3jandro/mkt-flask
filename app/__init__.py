from flask import Flask
from flask_bcrypt import Bcrypt
from flaskext.mysql import MySQL
from flask_jwt_extended import JWTManager

app = Flask(__name__)
mysql = MySQL()
app.config.from_object('config')
mysql.init_app(app)
bcrypt = Bcrypt(app)
jwt = JWTManager(app)


from app.controllers import campanha, lojas, ofertas, produtos, users
